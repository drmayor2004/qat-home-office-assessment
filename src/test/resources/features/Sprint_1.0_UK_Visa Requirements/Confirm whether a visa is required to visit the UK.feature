@Confirm_whether_a_visa_is_required_to_visit_the_UK

Feature: Confirm whether a visa is required to visit the UK
  Background:
  The organization/business wants to know whether the Application under test users from a specific country (as listed
  below) will be required a visa to visit or study in the UK.

    o Japan and Study
    o Japan and Tourism
    o Russia and Tourism



@Check_UK_Study_Visa_Requirement
  Scenario Outline: To check if user need a visa to study in the UK
    Given user is on the check uk visa page
    When user clicks on start now button
    Then What’s your nationality as shown on your passport or travel document? page should be displayed
    When user selects "<Country>" as nationality as shown on passport or travel document
    And  user clicks next step button
    Then What are you coming to the UK to do? page should be displayed
    When user selects "<Reason to visit UK>"
    And user clicks next step button
    Then check if you need a uk visa duration question page should be displayed
    When user selects "<Duration>" of study
    And user clicks next step button
    Then you will need a visa to study in the uk should be displayed
    Examples:
      |Country  |Reason to visit UK |Duration               |
      |Japan    |Study              |longer than 6 months   |


@UK_Tourim_Visa_Is_Not_Required
  Scenario Outline: To check if user would not need a visa to come to the UK
    Given user is on the check uk visa page
    When user clicks on start now button
    Then What’s your nationality as shown on your passport or travel document? page should be displayed
    When user selects "<Country>" as nationality as shown on passport or travel document
    And user clicks next step button
    Then What are you coming to the UK to do? page should be displayed
    When user selects "<Reason to visit UK>"
    And user clicks next step button
    Then You won’t need a visa to come to the UK should be displayed
    Examples:
      |Country  |Reason to visit UK |
      |Japan    |Tourism            |


  @UK_Tourim_Visa_Is_Required
  Scenario Outline: To check if user would need a visa to come to the UK
    Given user is on the check uk visa page
    When user clicks on start now button
    Then What’s your nationality as shown on your passport or travel document? page should be displayed
    When user selects "<Country>" as nationality as shown on passport or travel document
    And user clicks next step button
    Then What are you coming to the UK to do? page should be displayed
    When user selects "<Reason to visit UK>"
    And user clicks next step button
    Then Will you be travelling with or visiting either your partner or a family member in the UK? page should be displayed
    When user selects "<Answer>" as their response
    And user clicks next step button
    Then you will need a visa to come to the uk should be displayed
    Examples:
      |Country  |Reason to visit UK |Answer |
      |Russia    |Tourism           |No     |





