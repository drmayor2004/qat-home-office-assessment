package stepDefinitation;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import ukVisaPage.checkUkVisaPage.UkVisaRecordPage;


public class UkVisaPageSteps {



    @Steps
    UkVisaRecordPage ukVisaRecordPage;




    @Given("user is on the check uk visa page")
    public void userIsOnTheCheckUkVisaPage() {
        ukVisaRecordPage.theUkVisaHomePage();


    }

    @When("user clicks on start now button")
    public void userClicksOnStartNowButton() {
        ukVisaRecordPage.clickStartNow();
    }

    @Then("What’s your nationality as shown on your passport or travel document? page should be displayed")
    public void whatSYourNationalityAsShownOnYourPassportOrTravelDocumentPageShouldBeDisplayed() {
        ukVisaRecordPage.theNationalityPageIsDisplayed();
    }

    @When("user selects {string} as nationality as shown on passport or travel document")
    public void userSelectsAsNationalityAsShownOnPassportOrTravelDocument(String country) {
        ukVisaRecordPage.selectNationality(country);
    }

    @And("user clicks next step button")
    public void userClicksNextStepButton() {
        ukVisaRecordPage.clickNextStep();
    }

    @Then("What are you coming to the UK to do? page should be displayed")
    public void whatAreYouComingToTheUKToDoPageShouldBeDisplayed() {
        ukVisaRecordPage.reasonForComingToUKPageIsDisplayed();
    }

    @When("user selects {string}")
    public void userSelects(String reason) {
        ukVisaRecordPage.selectReasonForUkVisit(reason);
    }

     @Then("check if you need a uk visa duration question page should be displayed")
    public void checkIfYouNeedAUkVisaDurationQuestionPageShouldBeDisplayed() {
        ukVisaRecordPage.visaDurationPageIsDisplayed();
    }

    @When("user selects {string} of study")
    public void userSelectsOfStudy(String duration) {
        ukVisaRecordPage.visaDurationResponse(duration);

    }

    @Then("you will need a visa to study in the uk should be displayed")
    public void youWillNeedAVisaToStudyInTheUkShouldBeDisplayed() {
        ukVisaRecordPage.studyVisaIsRequiredIsDisplayed();
    }

    @Then("You won’t need a visa to come to the UK should be displayed")
    public void youWonTNeedAVisaToComeToTheUKShouldBeDisplayed() {
        ukVisaRecordPage.visaNotIsRequiredIsDisplayed();
    }

    @Then("Will you be travelling with or visiting either your partner or a family member in the UK? page should be displayed")
    public void willYouBeTravellingWithOrVisitingEitherYourPartnerOrAFamilyMemberInTheUKPageShouldBeDisplayed() {
        ukVisaRecordPage.travellingWithOrVisitingSomeone();
    }

    @When("user selects {string} as their response")
    public void userSelectsAsTheirResponse(String answer) {
        ukVisaRecordPage.userSelectsAsTheirResponse(answer);
    }

    @Then("you will need a visa to come to the uk should be displayed")
    public void youWillNeedAVisaToComeToTheUkShouldBeDisplayed() {
        ukVisaRecordPage.visaIsRequiredIsDisplayed();

    }
}
