package ukVisaPage.checkUkVisaPage;

import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.openqa.selenium.WebElement;


public class UkVisaRecordPage {
    UkVisaHomePageObjects ukVisaHomePageObjects;

    //xpath
    private static final String btn_START_NOW = "//a[@class='gem-c-button govuk-button govuk-button--start']";
    private static final String dd_NATIONALITY_AFGHANISTAN = "//option[@value='afghanistan']";
    private static final String dd_NATIONALITY_DROPDOWN = "//select[@id='response' and @class='govuk-select']";
    private static final String btn_NEXT_STEP = "//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']";
    private static final String lbl_REASON_FOR_COMING_TO_UK_TXT = "//legend[@class='govuk-fieldset__legend govuk-fieldset__legend--m']";
    private static final String rd_REASON_TO_VISIT_UK_RADIO_BTN = "//div[@class='govuk-radios']";
    private static final String rd_MORE_THAN_SIX_MONTH_RESPONSE = "//input[@class='govuk-radios__input' and @value='longer_than_six_months']";
    private static final String lbl_MORE_THAN_SIX_MONTH_TEXT = "//label[@class='gem-c-label govuk-label govuk-radios__label'][contains(text(),'longer than 6 months')]";
    private static final String lbl_LESS_THAN_SIX_MONTH_TEXT = "//label[@class='gem-c-label govuk-label govuk-radios__label'][contains(text(),'6 months or less')]";
    private static final String rd_LESS_THAN_SIX_MONTH_RESPONSE = "//input[@class='govuk-radios__input' and @value='six_months_or_less']";
    private static final String lbl_UK_VISA_DURATION_TXT = "//legend[@class='govuk-fieldset__legend govuk-fieldset__legend--m']";
    private static final String btn_ACCEPT_COOKIES = "//button[@class='gem-c-button govuk-button gem-c-button--inline']";
    private static final String lbl_VISA_REQUIREMENT_DECISION_TXT = "//h2[@class='gem-c-heading    govuk-!-margin-bottom-6']";
    private static final String lbl_TRAVELLING_WITH_OR_VISITING_SOMEONE_TXT = "//legend[@class='govuk-fieldset__legend govuk-fieldset__legend--m']";
    private static final String lbl_USER_SELECTS_AS_THEIR_RESPONSE_YES_TEXT = "//label[@class='gem-c-label govuk-label govuk-radios__label'][contains(text(),'Yes')]";
    private static final String lbl_USER_SELECTS_AS_THEIR_RESPONSE_NO_TEXT = "//label[@class='gem-c-label govuk-label govuk-radios__label'][contains(text(),'No')]";
    private static final String rd_USER_SELECTS_AS_THEIR_RESPONSE_YES_RADIO_BTN = "//input[@value='yes']";
    private static final String rd_USER_SELECTS_AS_THEIR_RESPONSE_NO_RADIO_BTN = "//input[@value='no']";


    @Step("Open the UK Visa Home Page")
    public Boolean theUkVisaHomePage() {
        ukVisaHomePageObjects.open();
        ukVisaHomePageObjects.findBy(btn_ACCEPT_COOKIES).click();
        String getTitlePage = ukVisaHomePageObjects.getTitle();
        System.out.println("Check UK Visa TitlePage: " + getTitlePage);
        Assert.assertEquals("Check if you need a UK visa - GOV.UK", getTitlePage);
        return true;

    }

    @Step("Click Start Now")
    public void clickStartNow(){
        ukVisaHomePageObjects.findBy(btn_START_NOW).click();
    }

    public Boolean theNationalityPageIsDisplayed() {
        String getTitlePage = ukVisaHomePageObjects.getTitle();
        System.out.println("NationalityPage: " + getTitlePage);
        Assert.assertEquals("Check if you need a UK visa - GOV.UK", getTitlePage);
        String getDropValue = ukVisaHomePageObjects.findBy(dd_NATIONALITY_AFGHANISTAN).getText();
        System.out.println("The first country on the nationlity dropdown: " + getDropValue);
        Assert.assertEquals("Afghanistan", getDropValue);
        return true;
    }

    public void selectNationality(String country) {

        ukVisaHomePageObjects.findBy(dd_NATIONALITY_DROPDOWN).selectByVisibleText(country);

        }

    public void clickNextStep() {
        ukVisaHomePageObjects.findBy(btn_NEXT_STEP).click();

    }

    public Boolean reasonForComingToUKPageIsDisplayed() {
        String getTitlePage = ukVisaHomePageObjects.getTitle();
        System.out.println("Reason for coming to UK page Title Page: " + getTitlePage);
        Assert.assertEquals("Check if you need a UK visa - GOV.UK", getTitlePage);
        String getLabelText = ukVisaHomePageObjects.findBy(lbl_REASON_FOR_COMING_TO_UK_TXT).getText();
        Assert.assertEquals("What are you coming to the UK to do?",getLabelText);
        System.out.println("User should see: " + getLabelText);
        return true;
    }

    public void selectReasonForUkVisit(String reason) {
        String selectedReason = reason.toLowerCase();
        System.out.println(selectedReason);
        String userOptionSelected = "//input[contains(@value,'"+selectedReason+"')]";
        WebElement optionToBeClicked = ukVisaHomePageObjects.findBy(rd_REASON_TO_VISIT_UK_RADIO_BTN).findBy(userOptionSelected);
        System.out.println(optionToBeClicked.getText());
        optionToBeClicked.click();
    }

    public Boolean visaDurationPageIsDisplayed() {
        String getTitlePage = ukVisaHomePageObjects.getTitle();
        System.out.println("UK Visa Duration Title Page: " + getTitlePage);
        Assert.assertEquals("Check if you need a UK visa - GOV.UK", getTitlePage);
        String getLabelText = ukVisaHomePageObjects.findBy(lbl_UK_VISA_DURATION_TXT).getText();
        System.out.println("User should see: " + getLabelText);
        Assert.assertEquals("How long are you planning to study in the UK for?", getLabelText);
        return true;

    }

    public void visaDurationResponse(String duration) {
        String durationSelected = ukVisaHomePageObjects.findBy(lbl_MORE_THAN_SIX_MONTH_TEXT).getText();
        if(duration.equals(durationSelected)){
            ukVisaHomePageObjects.findBy(rd_MORE_THAN_SIX_MONTH_RESPONSE).click();
            System.out.println("User Select Duration of: " + durationSelected );
        }
        else{
            durationSelected = ukVisaHomePageObjects.findBy(lbl_LESS_THAN_SIX_MONTH_TEXT).getText();
            ukVisaHomePageObjects.findBy(rd_LESS_THAN_SIX_MONTH_RESPONSE).click();
            System.out.println("User Select Duration of: " + durationSelected );
        }

    }

    public void studyVisaIsRequiredIsDisplayed() {
        String getTitlePage = ukVisaHomePageObjects.getTitle();
        System.out.println("Visa requirement result title page:  " + getTitlePage);
        Assert.assertEquals("Check if you need a UK visa - GOV.UK", getTitlePage);
        String getLabelText = ukVisaHomePageObjects.findBy(lbl_VISA_REQUIREMENT_DECISION_TXT).getText();
        Assert.assertEquals("You’ll need a visa to study in the UK", getLabelText );
        System.out.println(getLabelText);

    }

    public void visaNotIsRequiredIsDisplayed() {
        String getTitlePage = ukVisaHomePageObjects.getTitle();
        System.out.println("Visa requirement result title page:  " + getTitlePage);
        Assert.assertEquals("Check if you need a UK visa - GOV.UK", getTitlePage);
        String getLabelText = ukVisaHomePageObjects.findBy(lbl_VISA_REQUIREMENT_DECISION_TXT).getText();
        Assert.assertEquals("You won’t need a visa to come to the UK", getLabelText );
        System.out.println(getLabelText);
    }

    public void travellingWithOrVisitingSomeone() {
        String getTitlePage = ukVisaHomePageObjects.getTitle();
        System.out.println("Visa requirement result title page:  " + getTitlePage);
        Assert.assertEquals("Check if you need a UK visa - GOV.UK", getTitlePage);
        String getLabelText = ukVisaHomePageObjects.findBy(lbl_TRAVELLING_WITH_OR_VISITING_SOMEONE_TXT).getText();
        Assert.assertEquals("Will you be travelling with or visiting either your partner or a family member in the UK?", getLabelText );
        System.out.println(getLabelText);
    }

    public void userSelectsAsTheirResponse(String answer) {
        String responseOptionAvailable = ukVisaHomePageObjects.findBy(lbl_USER_SELECTS_AS_THEIR_RESPONSE_YES_TEXT).getText();
        System.out.println(responseOptionAvailable);
        if(answer.equals(responseOptionAvailable)){
            ukVisaHomePageObjects.findBy(rd_USER_SELECTS_AS_THEIR_RESPONSE_YES_RADIO_BTN).click();
        }
        else{
            responseOptionAvailable = ukVisaHomePageObjects.findBy(lbl_USER_SELECTS_AS_THEIR_RESPONSE_NO_TEXT).getText();
            System.out.println(responseOptionAvailable);
            ukVisaHomePageObjects.findBy(rd_USER_SELECTS_AS_THEIR_RESPONSE_NO_RADIO_BTN).click();
        }
    }

    public void visaIsRequiredIsDisplayed() {
        String getTitlePage = ukVisaHomePageObjects.getTitle();
        System.out.println("Visa requirement result title page:  " + getTitlePage);
        Assert.assertEquals("Check if you need a UK visa - GOV.UK", getTitlePage);
        String getLabelText = ukVisaHomePageObjects.findBy(lbl_VISA_REQUIREMENT_DECISION_TXT).getText();
        Assert.assertEquals("You’ll need a visa to come to the UK", getLabelText );
        System.out.println(getLabelText);
    }
}


