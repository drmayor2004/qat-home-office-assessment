# HOME OFFICE ASSESSMENT TEST SUMMARY REPORT
##Purpose
This document explains the various activities performed as part of testing of `Check UK visa website` (https://www.gov.uk/check-uk-visa/) application.

##Web Application Overview
Check UK visa website is one of the core integral functions of the home office website that allow users to check if a visa is required to study or to visit UK. 
The web application requires a user to provide information on their nationality, travel purposes (Tourism, Study etc), duration of their stay etc.

##Testing Scope
####a.	In Scope
Functional testing to Confirm whether a visa is required to visit the UK for the following:

•	Japan and Study 

•	Japan and Tourism 

•	Russia and Tourism 

####b.	Out of Scope
•	Functional Testing was not carried for other nationality (including purpose of travel) but the Automation Testing
 Framework was setup so it is scalable to include testing of these item in the future if the need arises.
 
•	Performance test was not done for this application

•	API testing was not carried out

####Types of Testing Performed
Based on the requirement specified in Intern Test Engineer (QAT) - Technical Assessment, only functional testing was carried out the application under test. 

The functional testing is required to validate the application/software under test against the functional requirements/specifications (Intern Test Engineer (QAT) - Technical Assessment). 
This will test each function of the application under test by providing valid input data and verifying and validating the output against the functional requirement.

The other type of testing such as Regression testing, Smoke testing, UAT testing, Sanity testing and other non-functional testing are out of scope. 


#Test Environment & Tools
|                        |                                |
|------------------------|-----------------------------------|
| Application URL        |https://www.gov.uk/check-uk-visa   |
| Programming Language   | Java                              |
| Project SDK version    | 1.8 (Java version 1.8_201)        |
| Project Name           | qat-home-office-assessment        |
| WebDriver              | Chrome                            |
| Report Output Location | report/Test Report/ index.html    |





# Get the Automation Test Code 

Git:

    git clone https://gitlab.com/mayowa_osewa/qat-home-office-assessment.git


Or simply [download a zip](https://gitlab.com/mayowa_osewa/qat-home-office-assessment/-/archive/master/qat-home-office-assessment-master.zip) file.


# The project directory structure
The project has build scripts for both Maven and Gradle, and follows the standard directory structure used in most Serenity projects:
```Gherkin
src
  + main
  + test
    + java                                                                   Test runners and supporting code
        + stepdefinition
          UkVisaPageSteps
        + ukVisaPage
          + checkUkVisaPage
            UkVisaHomePageObjects.java
            UkVisaRecordPage.java
        CucumberTestRunner                                                   Test Suite Runner
    + resources
      + features                                                             Feature files
        + uk_visa requirements                                               Feature file subdirectories 
          Confirm whether a visa is required to visit the UK.feature  
      + webdriver                   Bundled webdriver binaries
        + linux
        + mac
        + windows
          chromedriver.exe          OS-specific Webdriver binaries
          geckodriver.exe

```


#Check UK visa website scenario
This project uses Cucumber(Gherkin).
The feature file used where adapted (from the basic guidance provided in the requirement) and modified to include page verification and confirmation.

```Gherkin
Feature: Confirm whether a visa is required to visit the UK
  Background:
  The organization/business wants to know whether the Application under test users from a specific country (as listed
  below) will be required a visa to visit or study in the UK.
    o Japan and Study
    o Japan and Tourism
    o Russia and Tourism

    @you_will_need_a_visa_to_study_in_the_uk
    Scenario Outline: To check if user need a visa to study in the UK
        Given user is on the check uk visa page
        When user clicks on start now button
        Then what’s your nationality as shown on your passport or travel document? page should be displayed
        When user selects "<Nationality>" as shown on passport or travel document
        And  user clicks next step button
        Then what are you coming to the UK to do? page should be displayed
        When user selects "<Reason to visit UK>"
        And user clicks next step button
        Then check if you need a uk visa duration question page should be displayed
        When user selects "<Duration>" of study
        And user clicks next step button
        Then you will need a visa to study in the uk should be displayed

        Examples:
          |Nationality  |Reason to visit UK |Duration               |
          |Japan        |Study              |longer than 6 months   |


  @you_won’t_need_a_visa_to_come_to_the_UK
  Scenario Outline: To check if user would not need a visa to come to the UK
    Given user is on the check uk visa page
    When user clicks on start now button
    Then what’s your nationality as shown on your passport or travel document? page should be displayed
    When user selects "<Nationality>" as shown on passport or travel document
    And user clicks next step button
    Then what are you coming to the UK to do? page should be displayed
    When user selects "<Reason to visit UK>"
    And user clicks next step button
    Then you won’t need a visa to come to the uk should be displayed

    Examples:
      |Nationality  |Reason to visit UK |
      |Japan        |Tourism            |


  @you_will_need_a_visa_to_come_to_the_uk
  Scenario Outline: To check if user would need a visa to come to the UK
    Given user is on the check uk visa page
    When user clicks on start now button
    Then what’s your nationality as shown on your passport or travel document? page should be displayed
    When user selects "<Nationality>" as shown on passport or travel document
    And user clicks next step button
    Then what are you coming to the UK to do? page should be displayed
    When user selects "<Reason to visit UK>"
    And user clicks next step button
    Then Will you be travelling with or visiting either your partner or a family member in the UK? page should be displayed
    When user selects "<Answer>" as their response
    And user clicks next step button
    Then you will need a visa to come to the uk should be displayed

    Examples:
      |Nationality  |Reason to visit UK |Answer |
      |Russia       |Tourism            |No     |
```

# Executing the Tests
To run the project using either Maven command `mvn verify` or run the `CucumberTestRunner` test runner class
By default, the tests will run using Chrome however you can override webdriver at run time by using this Maven command:
```json
$ mvn clean verify -Ddriver=firefox
```

The test results will be recorded in the `report/Test Report` directory.

## Simplified WebDriver configuration and other configuration
This projects Serenity features which make configuring the tests easier. In particular, `serenity.conf` file in the `src/test/resources` directory to configure test execution options.
And also `serenity.properties` file in the `src/`  
### Webdriver configuration
The WebDriver configuration is managed entirely from this file `serenity.conf` file in the `src/test/resources`
The project also bundles some of the WebDriver binaries that you need to run Selenium tests in the `src/test/resources/webdriver` directories. These binaries are configured in the `drivers` section of the `serenity.conf` config file:

This configuration means that development machines and build servers do not need to have a particular version of the WebDriver drivers installed for the tests to run correctly.

### Environment-specific configurations
The test environment is configured to default (The requirement did not specified any environment). The other environments such as, _dev_, _staging_, _prod_, _SIT_, _UAT_ etc. with option to set different starting URLs for each:
```json
environments {
  default {
    webdriver.base.url = "https://www.gov.uk/check-uk-visa"
  }
//  dev {
//    webdriver.base.url = ""
//  }
//  staging {
//    webdriver.base.url = ""
//  }
//  prod {
//    webdriver.base.url = ""
//  }
//  SIT {
//    webdriver.base.url = ""
//  }
//  UAT {
//    webdriver.base.url = ""
//  }
```
  

#Test Result Metrics
| Testcases/Scenario Planned | Testcases/Scenario Executed | No. Testcases/Scenario Passed | No. Testcases/Scenario Failed |
|:--------------------------:|:---------------------------:|:-----------------------------:|:-----------------------------:|
|              3             |              3              |               3               |               0               |
|                            |                             |                               |                               |


![Test Result snapshot](https://gitlab.com/mayowa_osewa/qat-home-office-assessment/-/raw/HomeOfficeAssessment/report/images/resultsnapshot.png)


The detail test result (`index.html`)   file in the `report/Test Report/index.html`

#Proposed Exit Criteria

a) All test cases should be executed – Yes

b) All defects in Critical, Major, Medium severity should be verified and
closed – Yes. 

c) Any open defects in trivial severity – Action plan prepared with expected dates
of closure.

